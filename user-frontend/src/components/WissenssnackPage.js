import React from "react";
import "./css/Wissenssnack.css"
import unisport_logo from './images/logo_unisport.png';
import en_icon from './images/en_flag.png';
import de_icon from './images/de_flag.png';

class Wissenssnack extends React.Component{
    

    clickNext = () => {
        this.props.change_pos(this.props.cur_pos + 1)
      };
      clickBack = () => {
        this.props.change_pos(this.props.cur_pos - 1)
      };
    render(){
        return(

            <div className="mainContainer container padding">
                <div className ="row-eq-height row">
                    <div className = "app-body">
                        <div className ="header fluid">
                            <div className ="row">
			                    <div className = "col-2 col-sm-1 col-md-1 col-lg-1">
				                    <img
                                        style={this.props.lang === "en" ? {paddingBottom:"3px",borderBottom:"#99cc01 solid 5px"} : {}}
                                        className ="lang_switch_en"
                                        src ={en_icon}
                                        alt ="en_icon"
                                        onClick={this.props.toEnglish}
                                    />
				                </div>
			                    <div className = "col-2 col-sm-1 col-md-1 col-lg-1">
                                    <img
                                        style={this.props.lang === "de" ? {paddingBottom:"3px",borderBottom:"#99cc01 solid 5px"} : {}}
                                        className ="lang_switch_de"
                                        src ={de_icon}
                                        alt ="de_icon"
                                        onClick={this.props.toGerman}
                                    />
                                </div>
                                <div className ="col-4 col-sm-8 col-md-8 col-lg-8"></div>
                                <div className ="col-4 col-sm-2 col-md-2 col-lg-2">
                                    <img className="unisportLogo"
                                        src={unisport_logo} alt="unisport-logo"/>
                                </div>
                            </div>
                        </div>



                        <div className ="wiss-mainContent fluid">
                            <div className ="row">
                                <div className="col-sm-auto col-md-auto"></div>
                                <div className="col-12 col-sm-5 col-md-5" id ="circular">

                                    <h1 className="wissenssnack-text1">Wusstest Du...?</h1>
                                    <p className="wissenssnack-text2">{this.props.wissenssnackText}</p>

                                </div>
                                <div className="col-sm-auto col-md-auto"></div>
                                <div className="col-12 col-sm-6 col-md-6">
                                    <img className="image-test img-fluid center rounded center-block d-block mx-auto"
                                        id ="wiss-img" src={this.props.wissenssnackPhoto} alt='wiss-img'/>
                                </div>
                            </div>
                        </div>
                        <div className = "justify-content-between row" id = "footer">

                                <div className="col-md-2"></div>
                                <div className ="col-auto mr-auto">
                                    <button className = "btn btn-secondary"
                                        id = "backButton" type="button"
                                        onClick={this.clickBack}>Back</button>
                                </div>
                                <div className ="col-auto">
                                    <button className="btn btn-secondary"
                                        id="nextButton" type="button"
                                        onClick={this.clickNext} >
                                         Next
                                    </button>
                                </div>
                                <div className="col-md-2"></div>

                        </div>

                    </div>
                </div>

            </div>
        );
    }
}

export default Wissenssnack;