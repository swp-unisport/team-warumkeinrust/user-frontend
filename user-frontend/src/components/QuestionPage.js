
import React from 'react';
import Likert from 'react-likert-scale';
import "./css/QuestionPage.css"
import unisport_logo from './images/logo_unisport.png'
import en_icon from './images/en_flag.png';
import de_icon from './images/de_flag.png';

class Question extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            questionId : props.id,
            answerValue : props.answerValue,
            responses: props.responses,
            relevanceValue: props.relevanceValue,
            relevanceOptions: props.relevanceOptions,
            canGoFurther: (props.answerValue === -1) ? false : true,
            lastQuestion: (props.arrayLength === props.counter) ? true : false
        };

        this.updateAnswer = this.updateAnswer.bind(this);
        this.saveAnswerInStorage = this.saveAnswerInStorage.bind(this);
        this.updateRelevance = this.updateRelevance.bind(this);
        this.saveRelevanceInStorage = this.saveRelevanceInStorage.bind(this);
    }

    //Check the answer, cannot go to another question, if current question wasnt answered
    updateAnswer = e => {
        this.setState(prevState => ({
            answerValue : e.value,
            canGoFurther : true,

            responses: prevState.responses.map(
              r => r.value === e.value ? { ...r, checked: true} : { ...r, checked: false}
            )
          }));

    };
    saveAnswerInStorage = () => {
        sessionStorage.setItem("question"+this.state.questionId, this.state.answerValue);
    };

    updateRelevance = e => {
        this.setState(prevState => ({
            relevanceValue : e.value,
            relevanceOption: prevState.relevanceOptions.map(
              r => r.value === e.value ? { ...r, checked: true} : { ...r, checked: false}
            )
          }));
          console.log("your Answer is : ", e.value)
    };

    saveRelevanceInStorage = () => {
        sessionStorage.setItem("relevance"+this.state.questionId, this.state.relevanceValue);
    };

    clickNext = (evt) => {
        if (this.state.canGoFurther === true){
            this.saveAnswerInStorage();
            this.saveRelevanceInStorage();
            this.props.change_pos(this.props.cur_pos + 1)

            if (this.state.lastQuestion === true) {
                this.props.updateSportArten()
            }
        }
        else {
            alert("Please answer the question.")
        }

    };

    clickBack = () => {
        if (this.state.canGoFurther === true){
            this.saveAnswerInStorage();
            this.saveRelevanceInStorage();
            this.props.change_pos(this.props.cur_pos - 1)
        }
        else {
            alert("Please answer the question.")
        }
    };

    //Responds to the click of the finish button
    clickEnd = () => {
        if(this.state.lastQuestion === true){
            this.saveAnswerInStorage();
            this.saveRelevanceInStorage();
            this.props.change_pos(this.props.cur_pos + 1)

            var id = this.props.match.params.id;
            fetch('http://127.0.0.1:3000//'+id+'/',{
                method:'POST',
                body:JSON.stringify(this.state),
                headers:{
                    'Content-type': 'application/json; charset=UTF-8',
                },
            })
            .then(response=>response.json())
            .then((data)=>console.log(data));
        }
    };


    renderLikertAnswer() {
        console.log("answer likert")
        return (
            <div key={"answer"+this.state.questionId}>
                <Likert
                    responses = {this.state.responses}
                    onChange = {this.updateAnswer}

                />
            </div>
        )
    }

    renderLikertRelevance() {
        console.log("relevance likert")
        return (
            <div key={"relevance"+this.state.questionId}>
                <Likert
                    responses = {this.state.relevanceOptions}
                    onChange = {this.updateRelevance}
                />
            </div>
        )
    }

    render(){
        return(
                <div className="mainContainer container">
                    <div className="row justify-content-md-center">
                    <div className = "app-body">

                        <div className ="header fluid">
                            <div className ="row">
			                    <div className = "col-2 col-sm-1 col-md-1 col-lg-1">
				                    <img
                                        style={this.props.lang === "en" ? {paddingBottom: "3px",borderBottom:"#99cc01 solid 5px"} : {}}
                                        className ="lang_switch_en"
                                        src ={en_icon}
                                        alt ="en_icon"
                                        onClick={this.props.toEnglish}
                                    />
				                </div>
			                    <div className = "col-2 col-sm-1 col-md-1 col-lg-1">
                                    <img
                                        style={this.props.lang === "de" ? {paddingBottom: "3px",borderBottom:"#99cc01 solid 5px"} : {}}
                                        className ="lang_switch_de"
                                        src ={de_icon}
                                        alt ="de_icon"
                                        onClick={this.props.toGerman}
                                    />
                                </div>
                                <div className ="col-4 col-sm-8 col-md-8 col-lg-8"></div>
                                <div className ="col-4 col-sm-2 col-md-2 col-lg-2">
                                    <img className="unisportLogo"
                                        src={unisport_logo} alt="unisport-logo"/>
                                </div>
                            </div>
                        </div>

                        <div className ="mainContents fluid">
                            <div className ="row">
                                <div className ="col-12 col-sm-6">
                                    <div className ="questionSection">
                                        <p className="question-counter">Question {this.props.counter}/{this.props.arrayLength}</p>
                                        <h2 className="question-text1" >{this.props.questionText}</h2>
                                    </div>
                                </div>

                                <div className = "col-12 col-sm-6 answerSection">
                                    <div className="answerBox">
                                        {this.renderLikertAnswer()}
                                    </div>

                                    <div className = "question-relevance">
                                        <div className="justify-content-center row">
                                            <p id = "relevance-text">Wie wichtig ist Dir dieser Aspekt bei deiner Sportauswahl?</p>
                                            <div className = "relevance-likert">
                                                {this.renderLikertRelevance()}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className = "justify-content-between row" id ="footer">
                            <div className="col-md-2"></div>
                            <div className ="col-auto mr-auto">
                                <button className = "btn btn-secondary"
                                    id = "backButton" type="button"
                                    onClick={this.clickBack}>Back</button>
                            </div>
                            <div className ="col-auto">
                                <button className="btn btn-secondary"
                                    id="nextButton" type="button"
                                    onClick={this.clickNext} >
                                        {this.state.lastQuestion ? "Submit" : "Next"}
                                </button>
                            </div>
                            <div className="col-md-2"></div>
                        </div>

                    </div>
                    </div>
                </div>
        );
    }
}

export default Question;