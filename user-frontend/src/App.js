import React , { Component } from "react"
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import "./App.css"
import Welcome from "./components/WelcomeScreen.js";
import Question from "./components/QuestionPage.js";
import Activity from "./components/ActivityPage.js";
import Wissenssnack from "./components/WissenssnackPage.js";
import Result from "./components/ResultScreen";
import questionsResponses from "./components/jsonExamples/QuestionsResponses.json"
import relevanceResponses from "./components/jsonExamples/RelevanceResponses.json"



export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataArray : [],
      sportArtenArray : [],
      globalIndex: -1,
      lang : "de"
    };
    

    
    this.updateGlobalIndex = this.updateGlobalIndex.bind(this);
    this.updateSportArten = this.updateSportArten.bind(this);
    this.toEnglish = this.toEnglish.bind(this);
    this.toGerman = this.toGerman.bind(this);
    
  }



 
  toEnglish =() => {
    this.setState(
      {
        lang : "en"
      }
    )
  }
  toGerman =() =>{
    this.setState(
      {
        lang : "de"
      }
    )
  }

//---------------------------------------------------------------------------------fetching data 
  componentDidMount(){
    const request = {
      method: "GET",
    } 

    

    let serverAdress = "http://localhost:8000/api/user/quiz/"
     
    fetch(serverAdress, request)
      .then(response => response.json())
      .then(
        (result) => {
          console.log("result: ",result)
          this.setState({
            dataArray: result.order_list
          });
        },
        (error) => {
          console.log("error: " + error);
          this.setState({
            error
          });
        }
      );
  }
//---------------------------------------------------------------------------------
  gatherAnswers() {
    let outputData = []
    let questionIDlist = []

    //gather id for all fetched questions
  
    this.state.dataArray.forEach(d => {
      if (d.type ==="question") {
        questionIDlist.push(d.pk)
      }
    })
    questionIDlist.filter(Number)
    console.log("Question IDs: " + questionIDlist)

    //gather data about questions from sessionStorage and pack them 
  
    questionIDlist.forEach(qid => {
      let response = {};
      response.pk = qid;
      response.answer = parseInt(sessionStorage.getItem("question"+qid));
      response.relevance = parseInt(sessionStorage.getItem("relevance"+qid));
      outputData.push(response);
    })
    console.log("output: " + JSON.stringify(outputData));
    
    return outputData;
  }
  
//---------------------------------------------------------------------------------
  fetchSport(){
    let data = this.gatherAnswers()

    const request = {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    }
    
    

    let serverAdress = "http://localhost:8000/api/user/quiz/"
     
    fetch(serverAdress, request)
      .then(response => response.json())
      .then(
        (result) => {
          console.log("result: ",result)
          this.setState({
            sportArtenArray : result
          });
        },
        (error) => {
          console.log("error: " + error);
          this.setState({
            error
          });
        }
      );

  }

//---------------------------------------------------------------------------------
  updateGlobalIndex = (newValue) => {
    
    this.setState({ globalIndex : newValue })
    
  }

  updateSportArten = () => {
    this.fetchSport()
   
  }

  //getting total number of questions to render in counter-section        --no need until counter is fixed--

  getQuestionsTotalNumber = () => {
    let count = 0
    for(let i = 0 ; i < this.state.dataArray.length ; i++){
      if(this.state.dataArray[i].type === "question"){
        count++;
      }
    }
    return count;
  }


  //getting current position of the question from the dataArray, that should get rendered
  getCurrentQuestionIndex = (globalPositionOfQuestion) => {
    if(globalPositionOfQuestion < 0 || globalPositionOfQuestion >= this.state.dataArray.length){
      return -1
    }
    let count = 0
    for(let i = 0 ; i < this.state.dataArray.length ; i++){
      if(this.state.dataArray[i].type === "question"){
        count++;
        if(this.state.dataArray[i].pk === this.state.dataArray[globalPositionOfQuestion].pk){
          return count
        }
      }
    }
    return -1
  }

  getResponses = (id) => {
    let av = sessionStorage.getItem("question"+(id));
    console.log("responses were here " + id + ", value: " + av);
    let updatedResponses = questionsResponses;
    if (!(av === null)) {
        for (var i = 0; i < updatedResponses.length; i++) {
            if (updatedResponses[i].value === parseInt(av)) {
              updatedResponses[i].checked = true;
            }
            else {
                updatedResponses[i].checked = false;
            }
        }
    }
    else {
      for (let i = 0; i < updatedResponses.length; i++) {
            updatedResponses[i].checked = false;
      }
    }
    return updatedResponses;    
}

getAnswerValue = (id) => {
  let av = sessionStorage.getItem("question"+(id));
  console.log("value " + av)
  if (!(av === null)) {
    return parseInt(av);
  }
  else {
    return -1;
  }
}  

getRelevance = (id) => {
    let av = sessionStorage.getItem("relevance"+(id));
    console.log("importance were here " + id + ", value: " + av)
    let updatedRelevance = relevanceResponses;
    console.log("list: " + JSON.stringify(updatedRelevance))
    if (!(av === null)) {
        for (var i = 0; i < updatedRelevance.length; i++) {
            if (updatedRelevance[i].value === parseInt(av)) {
              updatedRelevance[i].checked = true;
            }
            else {
              updatedRelevance[i].checked = false;
          }
        }
    }
    else {
      for (let i = 0; i < updatedRelevance.length; i++) {
        if (updatedRelevance[i].value === 3) {
          updatedRelevance[i].checked = true;
        }
        else {
          updatedRelevance[i].checked = false;
        }
      }
    }

    return updatedRelevance;    
}

getRelevanceValue = (id) => {
  let av = sessionStorage.getItem("relevance"+(id));
  console.log("value " + av)
  if (!(av === null)) {
    return parseInt(av);
  }
  else {
    return 3;
  }
}


  renderContent() {
     //------------------------------------------------------------------------> show data to send 
    
      //if -1 render Welcome 
    let beginning = this.state.globalIndex < 0;
    let end = this.state.globalIndex >= this.state.dataArray.length
    
    if(beginning){
      return(
        <Welcome
          cur_pos={this.state.globalIndex}
          change_pos={this.updateGlobalIndex}
        />
      )
    }

    if (end) {
      //this.fetchSport()
      return(
        <Result
          cur_pos={this.state.globalIndex}
          change_pos={this.updateGlobalIndex}
          sportsList={this.state.sportArtenArray}
        />
      )
    }

    if (!beginning && !end) {

      if (this.state.dataArray[this.state.globalIndex].type === "question"){

        let currentQuestionIndex = this.getCurrentQuestionIndex(this.state.globalIndex)
        let totalLengthOfQuestionsArray = this.getQuestionsTotalNumber()
        return(
          <Question
            key={this.state.dataArray[this.state.globalIndex].pk}
            id={this.state.dataArray[this.state.globalIndex].pk}
            answerValue={this.getAnswerValue(this.state.dataArray[this.state.globalIndex].pk)}
            responses={this.getResponses(this.state.dataArray[this.state.globalIndex].pk)}
            relevanceValue = {this.getRelevanceValue(this.state.dataArray[this.state.globalIndex].pk)}
            relevanceOptions = {this.getRelevance(this.state.dataArray[this.state.globalIndex].pk)}
            change_pos={this.updateGlobalIndex}
            cur_pos={this.state.globalIndex}
            counter={currentQuestionIndex}
            arrayLength={totalLengthOfQuestionsArray}
            lang={this.state.lang}
            toGerman={this.toGerman}
            toEnglish={this.toEnglish}
            questionText={this.state.lang === "de" ? this.state.dataArray[this.state.globalIndex].text_de : this.state.dataArray[this.state.globalIndex].text_en}
            updateSportArten={this.updateSportArten}
          />
        )
      }
      //ACTIVITY/CALLTOMOVE COMPONENT 
      if(this.state.dataArray[this.state.globalIndex].type === "activity"){
        return(
          <Activity
            key={this.state.dataArray[this.state.globalIndex].pk}
            cur_pos={this.state.globalIndex}
            change_pos={this.updateGlobalIndex}
            lang={this.state.lang}
            toGerman={this.toGerman}
            toEnglish={this.toEnglish}
            activityText={this.state.lang === "de" ? this.state.dataArray[this.state.globalIndex].text_de : this.state.dataArray[this.state.globalIndex].text_en}
            activityPhoto={this.state.dataArray[this.state.globalIndex].url}
          />
        )
      }
      //WISSENSSNACK COMPONENT
      if(this.state.dataArray[this.state.globalIndex].type === "snack"){
        return(
          <Wissenssnack
            key={this.state.dataArray[this.state.globalIndex].pk}
            cur_pos={this.state.globalIndex}
            change_pos={this.updateGlobalIndex}
            lang={this.state.lang}
            toGerman={this.toGerman}
            toEnglish={this.toEnglish}
            wissenssnackText={this.state.lang === "de" ? this.state.dataArray[this.state.globalIndex].text_de : this.state.dataArray[this.state.globalIndex].text_en}
            wissenssnackPhoto={this.state.dataArray[this.state.globalIndex].url}
          />
        )
      }
    }
      
  }//end renderContent()
      
    
      

    
    

  render = () => {
    
    return (
      <div className="App">
        
        {/* {this.renderLanguageButtons()} */}
        {this.renderContent()}
        
      </div>
    );
  }

}
