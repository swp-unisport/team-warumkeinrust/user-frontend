import React from "react";
import unisport_logo from './images/logo_unisport.png';
import "./css/Activity.css";
import en_icon from './images/en_flag.png';
import de_icon from './images/de_flag.png';


class Activity extends React.Component{

    constructor(props){
        super(props)
        this.intervalInSeconds = 1
        this.state = {

            buttonIsDisabled : true,
            countDown : this.intervalInSeconds //change button-text while waiting
        }
    }

    clickNext = () => {
        this.props.change_pos(this.props.cur_pos + 1)
    };
    clickBack = () => {
    this.props.change_pos(this.props.cur_pos - 1)
    };

    //hier wird button solange deaktiviert bleiben bis die unten definierte Wartezeit um ist.
    waitUntilButtonActivatedMilSecond = 1000
    timer = setTimeout(() => {
        this.setState(
            {
                buttonIsDisabled : false,
            }
        )

    }, this.waitUntilButtonActivatedMilSecond); //here change waiting time : 500 = half second

    //hier dekrementieren wir den this.state.countDown um 1, damit DER auf dem Button erscheinende Text auch geändert wird.
    countDownInterval = 1000 // 1000 = 1 second
    componentDidMount(){
        setInterval(() => {
            this.setState(
                {
                    countDown : this.state.countDown - 1
                }
            )

        }, this.countDownInterval);
    }
    render(){
        return(

            <div className="mainContainer container-fluid-center">
                <div className="row">
                    <div className = "app-body">
                        <div className ="header fluid">
                            <div className ="row">
			                    <div className = "col-2 col-sm-1 col-md-1 col-lg-1">
				                    <img
                                        style={this.props.lang === "en" ? {paddingBottom:"3px",borderBottom:"#99cc01 solid 5px"} : {}}
                                        className ="lang_switch_en"
                                        src ={en_icon}
                                        alt ="en_icon"
                                        onClick={this.props.toEnglish}
                                    />
				                </div>
			                    <div className = "col-2 col-sm-1 col-md-1 col-lg-1">
                                    <img
                                        style={this.props.lang === "de" ? {paddingBottom:"3px",borderBottom:"#99cc01 solid 5px"} : {}}
                                        className ="lang_switch_de"
                                        src ={de_icon}
                                        alt ="de_icon"
                                        onClick={this.props.toGerman}
                                    />
                                </div>
                                <div className ="col-4 col-sm-8 col-md-8 col-lg-8"></div>
                                <div className ="col-4 col-sm-2 col-md-2 col-lg-2">
                                    <img className="unisportLogo"
                                        src={unisport_logo} alt="unisport-logo"/>
                                </div>
                            </div>
                        </div>

                        <div className ="activ-mainContent">
                            <div className ="row">

                                <div className="col-12 col-sm-6 col-md-6 col-lg-6" id ="act-img-field">
                                    <img className="img-fluid center rounded center-block d-block mx-auto"
                                        src={this.props.activityPhoto} alt='active_img' id ="act-img"/>
                                </div>
                                <div className="col-sm-auto col-md-auto col-lg-1"></div>
                                <div className="col-12 col-sm-5 col-md-5 col-lg-4" id ="textfield">
                                        <h2 className="activity-text1">Jetzt Wird's Aktiv!</h2>
                                        <p className="activity-text2">{this.props.activityText}</p>
                                </div>
                            </div>
                        </div>

                        <div className = "justify-content-between row" id ="act-footer">
                            <div className="col-md-2"></div>
                            <div className ="col-auto mr-auto">
                                <button className = "btn btn-secondary"
                                    id = "backButton"
                                    style={{cursor: this.state.buttonIsDisabled ? "wait" : "pointer"}}
                                    disabled={this.state.buttonIsDisabled ? true : false}
                                    onClick={this.clickBack}>
                                    {this.state.buttonIsDisabled ? "click after " + this.state.countDown : "Back"}
                                </button>
                            </div>
                            <div className ="col-auto">
                                <button className="btn btn-secondary"
                                    style={{ cursor: this.state.buttonIsDisabled ? "wait" : "pointer"}}
                                    disabled={this.state.buttonIsDisabled ? true : false}
                                    type="button" id="nextButton"
                                    onClick={this.clickNext}>
                                    {this.state.buttonIsDisabled ? "click after " + this.state.countDown : "Next"}
                                </button>
                            </div>
                            <div className="col-md-2"></div>
                        </div>

                </div>
                </div>

            </div>
        );
    }
}

export default Activity;