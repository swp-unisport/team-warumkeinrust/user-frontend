import React from "react";
import unisport_logo from './images/logo_unisport.png';
import {ListGroup} from 'react-bootstrap';
import "./css/ResultScreen.css"


class Result extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sport: this.props.sportsList
        }
    }

    clickBack = () => {
        this.props.change_pos(this.props.cur_pos - 1)
      };
    
    
    render(){
        return(
            
            <div className="result-container container">
                <div className ="header">
                    <div className ="row">
                        <div className ="col-8 col-sm-9 col-md-9 col-lg-8"></div>
                        <div className ="col-4 col-sm-3 col-md-3 col-lg-2">
                            <img className="unisportLogo"
                                    src={unisport_logo} alt="unisport-logo"/>
                        </div>
                        <div className ="col-lg-2"></div>
                    </div>
                </div>
                
                <div className = "resultMainContens">
                    <div className ="row">
                        <div className="col-lg-2 left-space"></div>
                        <div className="col-md-6 col-lg-4 left-messageText">
                            <h3 className='result-text1'>Aufgrund Deiner Angaben empfehlen wir Dir folgende Sportarten</h3>
                            <p className='result-text2'>Klick einfach auf die Sportart, um weiter Details zu erfahren!</p>
                            
                            <button className="btn btn-outline-secondary" type="button" 
                            id ="result-backButton" onClick={this.clickBack}>Back</button>
                            
                        </div>
                        
                        <div className="col-md-6 col-lg-4 result-list">
                          
                        <ListGroup className="result-sports-list">
                                <ListGroup.Item className ="list-title"> Perfekte Sporte für Dich</ListGroup.Item>
                                {/* sport list must be dynamic and extend by giving new new items in JSON*/}    
                                {
                                    this.props.sportsList.map((sportArg) => {

                                        let crit_one = "";
                                        if (sportArg.top_criteria[0] !== undefined) {
                                            crit_one = sportArg.top_criteria[0][0]
                                        }
                                        let crit_two = "";
                                        if (sportArg.top_criteria[1] !== undefined) {
                                            crit_two = sportArg.top_criteria[1][0]
                                        }
                                        let crit_three = "";
                                        if (sportArg.top_criteria[2] !== undefined) {
                                            crit_three = sportArg.top_criteria[2][0]
                                        }

                                        return (
                                            <ListGroup.Item className="active-item list-group-item-action flex-colum align-items-start" >
                                                <a href={sportArg.link} target="#"> {/* links missed for all result-sports */}
                                                    <div><p class="mb-2"><h4>{sportArg.name}</h4></p></div>

                                                    <span class="badge rounded-pill badge-result">{crit_one}</span>
                                                    <span class="badge rounded-pill badge-result">{crit_two}</span>
                                                    <span class="badge rounded-pill badge-result">{crit_three}</span>
                                                </a>
                                            </ListGroup.Item>
                                        )
                                    })
                                }

                            </ListGroup>
                        </div>
                    <div className="col-lg-2 right-space"></div>
                    </div>
                </div>
                
            </div>
        );
    }
}

export default Result;
