import React from "react";
import "./css/WelcomeScreen.css"
import project_logo from './images/logo_unisport_o_mat_755.png';





class Welcome extends React.Component{
    
    
    clickNext = () => {
        this.props.change_pos(this.props.cur_pos + 1)
      };

    render(){

        return(
            <div className = "welcome-bg">           
                <div class="center-of-page">

                    <div className="welcome-image row">
                        <div className="center rounded center-block d-block mx-auto"id="unisportSportOMatLogo">
                            <img className="img-fluid center rounded center-block d-block mx-auto" src={project_logo} alt="unisport-o-mat-logo"/>
                        </div>
                    </div>

                    <div className ="row">
                        <div id="welcome-text">
                            <p className="text-in-center">Beantworte die Fragen und finde heraus, welcher Sport zu dir am besten passt!</p>
                        </div>
                    </div> 
    
                        
                    <div className = "row">
                        <div className="col-sm-3 col-3"></div>
                        <div className="col-sm-6 col-6">
                            <button id="nextButton" className="btn btn-success w-100" type="button" onClick={this.clickNext}>Go!</button>
                        </div>    
                        <div class="col-sm-3 col-3"></div>
                    </div>    
                 
                </div>                 
            </div>
            
        );
    }
}

export default Welcome;
