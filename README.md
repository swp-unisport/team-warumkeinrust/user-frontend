# First steps to the UniSport-O-Mat Admin Frontend

## Installing `npm`
If not already installed, use your preferred way to install `npm`. It is recommended to install `npm` via `nvm`. A guide can be found [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) in the official `npm` documentation. 


## Installing dependencies
After cloning the repository, switch into the repository folder via terminal and type in 
```sh
npm install
```

## Start the development server
If you want to start the development server on the standard route, `localhost:3000`, use the following command

```sh
npm start
```

If you however wish to test the react app on another port, use
```sh
PORT=<port> npm start
```

## Testing your app
If you have configured test files to run, you can run them via terminal. More infos are located in the [official documentation](https://docs.npmjs.com/cli/v7/commands/npm-test). 
```sh
npm test
```